const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email address is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	billingAddress: [
		{
			blockLotApartmentNo: {
				type: String,
				required: [true, "Block/Lot/Apartment Number is required"]
			},
			street: {
				type: String,
				required: [true, "Street is required"]
			},
			subdBrgy: {
				type: String,
				required: [true, "Subdivision/Barangay is required"]
			},
			townCity: {
				type: String,
				required: [true, "Town/City is required"]
			},
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			zipCode: {
				type: String,
				required: [true, "Zip Code is required"]
			},
			country: {
				type: String,
				required: [true, "Country is required"]
			}
		}
	],
	shippingAddress: [
		{
			blockLotApartmentNo: {
				type: String,
				required: [true, "Block/Lot/Apartment Number is required"]
			},
			street: {
				type: String,
				required: [true, "Street is required"]
			},
			subdBrgy: {
				type: String,
				required: [true, "Subdivision/Barangay is required"]
			},
			townCity: {
				type: String,
				required: [true, "Town/City is required"]
			},
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			zipCode: {
				type: String,
				required: [true, "Zip Code is required"]
			},
			country: {
				type: String,
				required: [true, "Country is required"]
			}
		}
	],
	bought: [
		{
			orderId: {
				type: String,
				required: [true, "Order ID is required"]
			},
			paidOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);