const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	skus: [
		{
			sku: {
				type: String,
				required: [true, "SKU is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			feature: {
				type: String,
				required: [true, "Feature is required"]
			}
		}
	],
	addedOn: {
		type: Date,
		default: new Date()
	},
	isActive: {
		type: Boolean,
		default: true
	}
})


module.exports = mongoose.model("Product", productSchema);