const mongoose = require("mongoose");

const userPaymentSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	paymentType: {
		type: String,
		required: [true, "Payment type is required"]
	},
	status: {
		type: String,
		required: [true, "Status is required"]
	},
	cardCash: [
		{
			paymentMethod: {
				type: String,
				required: [true, "Payment method is required"]
			},
			lastFourNumbers: {
				type: String,
				required: [true, "Last four numbers of card is required"]
			},
			expiryMonth: {
				type: String,
				required: [true, "Expiry month is required"]
			},
			expiryYear: {
				type: String,
				required: [true, "Expiry year is required"]
			},
			cvvVerified: {
				type: Boolean,
				default: true
			}
		}
	]
})

module.exports = mongoose.model("userPayment", userPaymentSchema)