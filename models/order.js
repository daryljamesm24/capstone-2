const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	buyer: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			boughtOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	paymentId: {
		type: String,
		required: [true, "Payment ID is required"]
	},
	paymentStatus: {
		type: String,
		required: [true, "Payment status is required"]
	},
	status: {
		type: String,
		required: [true, "Status is required"]
	},
	amount: {
		type: Number,
		required: [true, "Amount is required"]
	},
	items: [
		{
			sku: {
				type: String,
				required: [true, "SKU is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			discount: {
				type: Number,
				required: [true, "Discount is required"]
			},
			tax: {
				type: Number,
				required: [true, "Tax is required"]
			}
		}
	],
	shippingAddress: [
		{
			blockLotApartmentNo: {
				type: String,
				required: [true, "Block/Lot/Apartment Number is required"]
			},
			street: {
				type: String,
				required: [true, "Street is required"]
			},
			subdBrgy: {
				type: String,
				required: [true, "Subdivision/Barangay is required"]
			},
			townCity: {
				type: String,
				required: [true, "Town/City is required"]
			},
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			zipCode: {
				type: String,
				required: [true, "Zip Code is required"]
			},
			country: {
				type: String,
				required: [true, "Country is required"]
			}
		}
	],
	billingAddress: [
		{
			blockLotApartmentNo: {
				type: String,
				required: [true, "Block/Lot/Apartment Number is required"]
			},
			street: {
				type: String,
				required: [true, "Street is required"]
			},
			subdBrgy: {
				type: String,
				required: [true, "Subdivision/Barangay is required"]
			},
			townCity: {
				type: String,
				required: [true, "Town/City is required"]
			},
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			zipCode: {
				type: String,
				required: [true, "Zip Code is required"]
			},
			country: {
				type: String,
				required: [true, "Country is required"]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);