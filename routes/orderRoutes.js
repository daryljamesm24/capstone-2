const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController")

router.post("/register", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.registerOrder(req.body).then(resultFromOrder => res.send(resultFromOrder))
})

router.get("/details", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){

	orderController.getOrder().then(resultFromGetProfile => res.send(resultFromGetProfile))
	}else{
		res.send({auth: "Not an admin"})
	}
})

router.get("/myOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	orderController.getMyOrder(userData.id).then(resultFromMyOrder => res.send(resultFromMyOrder))
})

module.exports = router