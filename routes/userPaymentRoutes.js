const express = require("express");
const router = express.Router();
const userPaymentController = require("../controllers/userPaymentController")

router.post("/register", (req, res) => {
	userPaymentController.paymentUser(req.body).then(resultFromPayment => res.send(resultFromPayment))
})

module.exports = router