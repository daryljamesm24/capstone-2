const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

router.post("/register", (req, res) => {
	productController.registerProduct(req.body).then(resultFromProduct => res.send(resultFromProduct))
})

router.get("/all", (req, res) => {
	productController.allProduct().then(resultFromAll => res.send(resultFromAll))
})

router.get("/:productId", (req, res) => {
	console.log(req.params)
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

router.post("/create", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
	
})

router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){

	productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an admin"})
	}
})

router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){

	productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})


module.exports = router