const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController")

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

router.post("/purchase", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId
	}

	userController.purchase(data).then(resultFromEnroll => res.send(resultFromEnroll))
})


module.exports = router