const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require('./routes/userRoutes');
const userPaymentRoutes = require('./routes/userPaymentRoutes');
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require('./routes/orderRoutes')

const app = express();
const port = 3000

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));

app.use("/users", userRoutes)
app.use("/payment", userPaymentRoutes)
app.use("/products", productRoutes)
app.use("/order", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.uabj3.mongodb.net/b110_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})