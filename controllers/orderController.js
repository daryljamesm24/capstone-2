const Order = require("../models/order");
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.registerOrder = (body) => {
	let newOrder = new Order({
		//userId: body.userId,
		paymentId: body.paymentId,
		paymentStatus: body.paymentStatus,
		status: body.status,
		amount: body.amount,
		items: {
			sku: body.items.sku,
			quantity: body.items.quantity,
			price: body.items.price,
			discount: body.items.discount,
			tax: body.items.tax
		},
		shippingAddress: {
			blockLotApartmentNo: body.shippingAddress.blockLotApartmentNo,
			street: body.shippingAddress.street,
			subdBrgy: body.shippingAddress.subdBrgy,
			townCity: body.shippingAddress.townCity,
			province: body.shippingAddress.province,
			zipCode: body.shippingAddress.zipCode,
			country: body.shippingAddress.zipCode
		},
		billingAddress: {
			blockLotApartmentNo: body.billingAddress.blockLotApartmentNo,
			street: body.billingAddress.street,
			subdBrgy: body.billingAddress.subdBrgy,
			townCity: body.billingAddress.townCity,
			province: body.billingAddress.province,
			zipCode: body.billingAddress.zipCode,
			country: body.billingAddress.zipCode
		}
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.getOrder = () => {
	return Order.find().then(result => {
		return result;
	})
}


module.exports.getMyOrder = (userData) => {
	console.log(userData)
	return Order.findById({buyer: userData._userId}).then(result => {
		return result;
	})
}