const Product = require("../models/product");
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.registerProduct = (body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		skus:
			{
				sku: body.skus.sku,
				price: body.skus.price,
				quantity: body.skus.quantity,
				feature: body.skus.feature
			}
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.allProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (params) => {
	console.log(params)
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		skus:
			{
				sku: body.skus.sku,
				price: body.skus.price,
				quantity: body.skus.quantity,
				feature: body.skus.feature
			}
	})

	return newCourse.save().then((course, error) => {
		if(error){
			return false; //save unsuccessful
		}else{
			return true; //save successful
		}
	})

}


module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		skus:
			{
				sku: body.skus.sku,
				price: body.skus.price,
				quantity: body.skus.quantity,
				feature: body.skus.feature
			}
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})  
}