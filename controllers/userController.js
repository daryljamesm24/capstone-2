const User = require("../models/user");
const Order = require("../models/order")
const bcrypt = require("bcrypt")
const auth = require("../auth")


module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNumber: body.mobileNumber,
		billingAddress: {
			blockLotApartmentNo: body.billingAddress.blockLotApartmentNo,
			street: body.billingAddress.street,
			subdBrgy: body.billingAddress.subdBrgy,
			townCity: body.billingAddress.townCity,
			province: body.billingAddress.province,
			zipCode: body.billingAddress.zipCode,
			country: body.billingAddress.zipCode
		},
		shippingAddress: {
			blockLotApartmentNo: body.shippingAddress.blockLotApartmentNo,
			street: body.shippingAddress.street,
			subdBrgy: body.shippingAddress.subdBrgy,
			townCity: body.shippingAddress.townCity,
			province: body.shippingAddress.province,
			zipCode: body.shippingAddress.zipCode,
			country: body.shippingAddress.zipCode
		}
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false; //User doesn't exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false; //password didn't match; can't log in
			}
		}
	})
}

module.exports.getProfile = (userId) => {
	console.log(userId)
	return User.findById(userId).then(result => {
		result.password = undefined;
		return result;
	})
}

module.exports.purchase = async (data) => {
console.log(data)
	let orderSaveStatus = await Order.findById(data.orderId).then(order => {
		order.buyer.push({userId: data.userId})
		return order.save().then((order, err) => {
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})

	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.bought.push({orderId: data.orderId})
		return user.save().then((user, err) => {
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})

	if(orderSaveStatus && userSaveStatus){
		return true;
	}else{
		return false;
	}
}
