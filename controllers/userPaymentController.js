const Payment = require("../models/userPayment");
const bcrypt = require("bcrypt")

module.exports.paymentUser = (body) => {
	let newPayment = new Payment({
		userId: body.userId,
		paymentType: body.paymentType,
		status: body.status,
		cardCash: {
			paymentMethod: body.cardCash.paymentMethod,
			lastFourNumbers: body.cardCash.lastFourNumbers,
			expiryMonth: body.cardCash.expiryMonth,
			expiryYear: body.cardCash.expiryYear
		}
		// 10 in the above represents "salt." (the number of times it will run the encryption)
	})

	return newPayment.save().then((payment, error) => {
		if(error){
			return false; 
		}else{
			return true;
		}
	})
}